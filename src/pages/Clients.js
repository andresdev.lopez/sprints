import React from 'react';
import ClientsTable from '../components/ClientsTable';
import styles from './styles/Clients.css'; 
import NavigationBar from '../components/NavigationBar';
import ClientForm from '../components/ClientForm';

const Clients = () => {


  return (
    <div className="Clients">

      <NavigationBar/>
      
      <ClientForm/>
      
      <ClientsTable />
      

    </div >
  );

}

export default Clients;