import './App.css';
import { Router, Route, browserHistory } from 'react-router';
import Home from './pages/Home';
import Clients from './pages/Clients';
import Products from './pages/Products';

function App() {
  return (
    <Router history={browserHistory}>
      <Route path="/" component={Home}></Route>
      <Route path="/home" component={Home}></Route>
      <Route path="/clients" component={Clients}></Route>
      <Route path="/products" component={Products}></Route>
    </Router>
  );
}

export default App;
