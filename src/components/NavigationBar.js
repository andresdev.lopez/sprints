import React from 'react'

//Bootstrap components
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import logo from '../assets/Logo-Final.png';


const NavigationBar = () => {

  return (
    <Navbar bg="dark" variant="dark">

      <img className="m-3 h-12 w-auto sm:h-12" src={logo} alt="site"/>

      <Nav variant="pills">
        <Nav.Item>
          <Nav.Link href="/home">Home</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href="/products">Productos</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href="/clients">Clientes</Nav.Link>
        </Nav.Item>
      </Nav>

    </Navbar>
  )
}

export default NavigationBar