import React, {useState} from 'react'
import Form from 'react-bootstrap/Form'
import { Col, InputGroup, FormControl, Row, Button } from 'react-bootstrap';


const ClientForm = () => {

    const [user, setUser] = useState({
        nombre: '',
        cedula: '',
        telefono: '',
        correo: ''
    })

    const handleImputChange = (event) => {
        setUser({
            ...user,
            [event.target.name] : event.target.value
        })
    }

    const registrarUsuario = (event) => {
        event.preventDefault()
        console.log(`Usuario registrado con los siguientes datos: NOMBRE: ${user.nombre} CEDULA: ${user.cedula} TELEFONO: ${user.telefono} CORREO: ${user.correo}`)
    }

    return (
        <Form onSubmit={registrarUsuario}>
            <Row className="align-items-center">
                <Col sm={3} className="my-1">
                    <Form.Label htmlFor="inlineFormInputName" visuallyHidden>
                        Nombre
                    </Form.Label>
                    <Form.Control onChange={handleImputChange} name="nombre" placeholder="Nombre" />
                </Col>
                <Col sm={3} className="my-1">
                    <Form.Label htmlFor="inlineFormInputName" visuallyHidden>
                        Cedula
                    </Form.Label>
                    <Form.Control onChange={handleImputChange} name="cedula" placeholder="Cedula" />
                </Col>
                <Col sm={3} className="my-1">
                    <Form.Label htmlFor="inlineFormInputName" visuallyHidden>
                        telefono
                    </Form.Label>
                    <Form.Control onChange={handleImputChange} name="telefono" placeholder="telefono" />
                </Col>
                <Col sm={3} className="my-1">
                    <Form.Label htmlFor="inlineFormInputGroupUsername" visuallyHidden>
                        Correo
                    </Form.Label>
                    <InputGroup>
                        <InputGroup.Text>@</InputGroup.Text>
                        <FormControl onChange={handleImputChange} name="correo" placeholder="Correo" />
                    </InputGroup>
                </Col>
                <Col xs="auto" className="my-1">
                    <Button type="submit">Registrar cliente</Button>
                </Col>
            </Row>
        </Form>
    );
}

export default ClientForm
